if exists('g:loaded_ProjectBuilder')
    finish
endif
let g:loaded_ProjectBuilder = 1

"
" Detect build system for current buffer
"
function s:DetectBuildSystem()
    " Variables has s: and not b: scope, because make and cwindow can change current buffer. So
    " cannot be run in parallel (i dont now if it is possible in vim)
    let s:qmake_projects = split(glob("*.pro"))
    let s:build_system = "none"

    if filereadable("Makefile")
        let s:build_system = "make"
    elseif filereadable("CMakeLists.txt")
        let s:build_system = "cmake"
    elseif len(s:qmake_projects) > 0
        let s:build_system = "qmake"
    else
    endif
endfunction


"
" Helper functions - check for concrete build system
"
function s:IsMake()
    return s:build_system ==# "make"
endfunction

function s:IsCmake()
    return s:build_system ==# "cmake"
endfunction

function s:IsQmake()
    return s:build_system ==# "qmake"
endfunction


"
" Configure project
"
function s:ConfigureProject(...)
    if (s:IsQmake() || s:IsCmake()) && (!isdirectory("build"))
        call mkdir("build", "p")
        if s:IsQmake()
            cd build
            " We are using only first *.pro file
            execute "silent ! qmake " . join(a:000) . " ../" . s:qmake_projects[0]
            cd -
        elseif s:IsCmake()
            cd build
            execute "silent ! cmake " . join(a:000) . " ../"
            cd -
        endif
    endif
endfunction


"
" Compile project
"
function s:CompileProject(...)
    if s:IsMake()
        execute "make " . join(a:000)
    elseif s:IsQmake() || s:IsCmake()
        cd build
        execute "make " . join(a:000)
        cd -
    endif
endfunction


"
" Build project
"
function BuildProject(...)
    silent ! clear

    call s:DetectBuildSystem()

    if s:build_system ==# "none"
        redraw!
        echoerr "ProjectCompiler error: no build system found"
    else
        call s:ConfigureProject(join(a:000))
        call s:CompileProject()
        redraw!
    endif

    cwindow
endfunction


"
" Rebuild project
"
function RebuildProject(...)
    silent ! clear

    call s:DetectBuildSystem()

    if (s:IsQmake() || s:IsCmake()) && (isdirectory("build"))
        execute "silent ! rm -rf build"
    endif

    call BuildProject(join(a:000))
endfunction


"
" Test project
"
function TestProject(...)
    silent ! clear

    call BuildProject(join(a:000))

    " We expect that BuildProject calls s:SetectBuildSystem
    call s:CompileProject("check")

    redraw!
    cwindow
endfunction

command -nargs=* BuildProject call BuildProject(<f-args>)
command -nargs=* RebuildProject call RebuildProject(<f-args>)
command -nargs=* TestProject call TestProject(<f-args>)
